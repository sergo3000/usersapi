const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name:String,
    health:Number,
    attack:Number,
    defense:Number,
    source:String
})


module.exports = mongoose.model('Users', userSchema)