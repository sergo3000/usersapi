const Users = require('../models/userModel');
class DbStrategy{
    async add({name, health, attack, defense, source}){
        try {
            let user = new Users({
                name:name,
                health:health,
                attack:attack,
                defense:defense,
                source:source
            });
            let u = await user.save()
            return u;
        } catch (err) {
            console.log(err)
            return null;
        }
    }
    async getOne(id){
        try {
            let user = await Users.findById(id)
            return user;
        } catch (err) {
            console.log(err)
            return null;
        }
    }
    async getAll(){
        try {
            let users = await Users.find({});
            return users;
        } catch (err) {
            console.log(err)
            return null;
        }
    }
    async edit(id, {name, health, attack, defense, source}){
        try {

            let user = await Users.findById(id)
            user.name = name||user.name
            user.health = health||user.health
            user.attack = attack||user.attack
            user.defense = defense||user.defense
            user.source = source||user.source
            await user.save()
            return user
        } catch (err) {
            console.log(err)
            return null
        }
    }
    async delete(id){
        try {
            let user = await Users.deleteOne({_id : id})
            return user;
        } catch (err) {
            console.log(err);
            return null;
        }
    }
}
module.exports = DbStrategy;