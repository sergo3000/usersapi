const LocalFileStrategy = require('./localFileStrategy');
const DbStrategy = require('./dbStrategy');
class UsersProvider{

    constructor(ds =  LocalFileStrategy) {
        this.dataStrategy = new ds();
    }
    add(obj){
        return this.dataStrategy.add(obj);
    }
    getOne(id){
        return this.dataStrategy.getOne(id);
    }
    getAll(){
        return this.dataStrategy.getAll();
    }
    edit(id, obj){
        return this.dataStrategy.edit(id, obj);
    }
    delete(id){
        return this.dataStrategy.delete(id);
    }
}
module.exports = UsersProvider;
