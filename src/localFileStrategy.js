const fs = require('fs')
const User = require('../models/userModel');
const path = require('path');

class LocalFileStrategy{ 
    constructor() {
        this.filePath = path.join(__dirname, '../data/users.json')
        
    }
    async add({name, health, attack, defense, source}){
        try {
            let user = new User({
                name:name,
                health:health,
                attack:attack,
                defense:defense,
                source:source
            });

            let users = await this.getAll()
            users.push(user)
            this.writeData(users)
            return user;
        } catch (err) {
            console.log(err)
            return null;
        }
    }
    async getOne(id){
        try {
            let users = await this.getAll()
            let user = users.find(u=>u._id == id);
            return user || null
        } catch (err) {
            console.log(err)
            return null;
        }
    }
    async getAll(){
        try {
            let users = await this.readData(this.fileName)
            return users 
        } catch (err) {
            console.log(err)
            return null;
        }
    }
    async edit(id, {name, health, attack, defense, source}){
        try {
            let users = await this.getAll()
            let userIndex = users.findIndex(u=>u._id == id);
            if(userIndex >= 0){
                users[userIndex].name =  name || users[userIndex].name
                users[userIndex].health = health || users[userIndex].health
                users[userIndex].attack = attack || users[userIndex].attack
                users[userIndex].defense =  defense || users[userIndex].defense
                users[userIndex].source =  source || users[userIndex].source           

                this.writeData(users)
                return users[userIndex]
            }
            else{
                return null
            }
        } catch (err) {
            console.log(err)
            return null
        }
    }
    async delete(id){
        try {
            let users = await this.getAll()
            let userIndex = users.findIndex(u=>u._id == id);
            if(userIndex >= 0){
                users.splice(userIndex,1)
                this.writeData(users)
                return true
            }
            else{
                return false
            }
        } catch (err) {
            console.log(err)
            return null
        }
    }
    readData(){
        let rawdata = fs.readFileSync(this.filePath );  
        let data = JSON.parse(rawdata)
        return Promise.resolve(data)
    }
    writeData(data){
        fs.writeFileSync(this.filePath, JSON.stringify(data))
    }
}
module.exports = LocalFileStrategy;