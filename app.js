const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

// database settings
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const dbUri = 'mongodb+srv://dbUser:12345@cluster0-qebsz.mongodb.net/usersDB?retryWrites=true'
mongoose.connect(dbUri,{
  useNewUrlParser:true
  //,useMongoClient:true
}).then(() => {
    console.log("MongoDB has started...");
  })
  .catch((err) => {
    //throw new Error("db crash");
    console.log(err)
    console.log('db crash')
  })


const indexRouter = require('./routes/index');
const usersRouter = require('./routes/user');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/user', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});
app.listen(3000)
module.exports = app;
