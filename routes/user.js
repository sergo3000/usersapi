const express = require('express')
const router = express.Router()
const UsersProvider = require('../src/usersProvider')

const usersProvider =  new UsersProvider()

/* GET users listing. */
router.get('/', async function(req, res) {
    let users = await usersProvider.getAll()
    if(users){
      res.send(users)
    }
    else{
      res.status(400).send("something goes ne tak")
    }
});

router.get('/:id',async (req, res) => {
    let user = await usersProvider.getOne(req.params.id)
    if(user){
      res.send(user)
    }
    else{
      res.status(400).send("user not found")
    }
})

router.delete('/delete/:id',async (req, res) => {
    
    let user = await usersProvider.delete(req.params.id)
    if(user){
      res.send("user deleted")
    }
    else{
      res.status(400).send("user not deleted")
    }
})

router.post('/', async (req, res) => {
    let newUser = await usersProvider.add(req.body)  
    if (newUser) {
      res.send(newUser)
    } else {
      send.status(400).send('user not created')
    }
})

router.put('/:id', async (req, res) => {
  //5ce8677170b5d5348c595d76
    let newUser = await usersProvider.edit(req.params.id, req.body)  
    if (newUser) {
      res.send(newUser)
    } else {
      send.status(400).send('user not edited')
    }
})



module.exports = router
